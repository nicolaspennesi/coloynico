<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Decode JSON
$_POST = json_decode(file_get_contents("php://input"),true);

// Set variables
$name = !empty($_POST['name']) ? $_POST['name'] : '';
$email = !empty($_POST['email']) ? $_POST['email'] : '';
$gift = !empty($_POST['gift']) ? $_POST['gift'] : '';
$quantity = !empty($_POST['quantity']) ? $_POST['quantity'] : '0';
$price = !empty($_POST['price']) ? $_POST['price'] : '';
$payWithCash = !empty($_POST['payWithCash']) ? true : false;
$message = !empty($_POST['message']) ? $_POST['message'] : '';

//Load Composer's autoloader
require '../vendor/autoload.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.coloynico.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'web@coloynico.com';                     //SMTP username
    $mail->Password   = 'WebColoYNico';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom('web@coloynico.com', 'Web Casamiento');
    $mail->addAddress('nicolaspennesi@gmail.com', 'Nicolás Pennesi');
    // $mail->addAddress('rociobonade@gmail.com', 'Rocío Bonadé');

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->CharSet = 'UTF-8';
    $mail->Subject = $name . ' mandó un regalo!';
    $mail->Body = 'Nombre: ' . $name . '<br>';
    $mail->Body .= 'Email: ' . $email . '<br>';
    $mail->Body .= 'Regalo: ' . $gift . ' (x' . $quantity . ')<br>';
    if ($payWithCash) {
        $mail->Body .= 'Total: $' . $quantity * $price . ' (lleva sobre al casamiento)<br>';
    } else {
        $mail->Body .= 'Total: $' . $quantity * $price . '<br>';
    }
    $mail->Body .= 'Mensaje: ' . $message;
    $mail->send();

    echo 'Mensaje enviado. Muchas gracias! :)';
} catch (Exception $e) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    echo "Error al enviar mensaje.";
}
